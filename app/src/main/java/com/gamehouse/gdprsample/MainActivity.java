package com.gamehouse.gdprsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.gamehouse.gdpr.GDPRActivity;
import com.gamehouse.gdpr.GDPRActivityEnum;
import com.gamehouse.gdpr.GamehouseGDPR;
import org.json.JSONException;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "GamehouseGDPR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //GDPR check
        if (!GamehouseGDPR.getInstance().validate(this)){
            /*
                This creates the consent
            */
            createGDPRDialog(GDPRActivityEnum.GDPR_CONSENT);

            /*
                This creates the opt-out
            */
            //createGDPRDialog(GDPRActivityEnum.GDPR_OPT_OUT);

            /*
                This creates the ask-again
            */
            //createGDPRDialog(GDPRActivityEnum.GDPR_ASK_AGAIN);

        }
        else
        {
            try {
                initAllSDKs();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /*
        This method can be use to initialize all the SDKs
     */
    public void initAllSDKs() throws UnsupportedEncodingException, JSONException {
        //We had the consent before, init with all the SDKS
        Log.d("GDPR", "[GDPR] all the sdks init....");
        GamehouseGDPR.getInstance().gdprUpdateBackend(true, "18b380e5-4369-4bc4-a05b-03c1f2b58a8f"); //fake global id
    }

    public void optOutUser() throws UnsupportedEncodingException, JSONException
    {
        GamehouseGDPR.getInstance().gdprUpdateBackend(false, "18b380e5-4369-4bc4-a05b-03c1f2b58a8f"); //fake global id
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GamehouseGDPR.getInstance().GDPR_ACTIVITY_CODE) {
            //User did consent
            if (resultCode == GDPRActivity.GDPR_CONSENT_OK) {
                Log.d(TAG, "[GDPR][GamehouseGDPR] Received positive from the user! ");
                try {
                    initAllSDKs();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //User did not conset
            if(resultCode == GDPRActivity.GDPR_CONSENT_NOK){
                Log.d(TAG, "[GDPR][GamehouseGDPR] User did not accept");
                createGDPRDialog(GDPRActivityEnum.GDPR_ASK_AGAIN);
                //this.finish();
                //System.exit(0);
            }
            //User pressed opt-out method
            if(resultCode == GDPRActivity.GDPR_OPT_OUT_OK){
                Log.d(TAG, "[GDPR][GamehouseGDPR] User opted out");
                try {
                    optOutUser();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //User pressed No but we want to ask again
            if(resultCode == GDPRActivity.GDPR_ASK_AGAIN_OK){
                createGDPRDialog(GDPRActivityEnum.GDPR_CONSENT);
            }
        }
    }

    private void createGDPRDialog(GDPRActivityEnum typeOfDialog){
        Intent intent = new Intent(this, com.gamehouse.gdpr.GDPRActivity.class);
        typeOfDialog.attachTo(intent);
        startActivityForResult(intent, GamehouseGDPR.getInstance().GDPR_ACTIVITY_CODE);
    }
}
