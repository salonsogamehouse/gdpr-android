package com.gamehouse.gdpr;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import cz.msebera.android.httpclient.entity.StringEntity;


public class GamehouseGDPR extends Application {

    private static final String TAG = GamehouseGDPR.class.getSimpleName();
    private static GamehouseGDPR ourInstance = null;
    private static String urlGDPR = "https://api.test.gamehouseoriginalstories.com/playerprofile/consent/";

    public static final int GDPR_ACTIVITY_CODE = 1;

    /*
       Manager class instance to access it from outside
     */
    public static GamehouseGDPR getInstance() {
        if(ourInstance == null) {
            ourInstance = new GamehouseGDPR();
        }
        return ourInstance;
    }

    /*
        Returns if we have already store the consent in the SharedPreferences
        @param_in: receives an activity usually the caller.
     */
    public Boolean validate(Activity activity) {
        if (GDPRStorage.getGdprConsent(activity.getApplicationContext())) {
            Log.d(TAG, "[GDPR][GamehouseGDPR] Had the consent before, so nothing to do here...");
            return true;

        } else {
            Log.d(TAG, "[GDPR][GamehouseGDPR] Consent not found, so asking the user for consent");
            return false;
        }
    }

    /*
        This method updates the GH backend for a specific globalID.
        @param_in: consent in or opt-out
        @param_in: globalID from the ghsdk
     */
    public void gdprUpdateBackend(Boolean consent, String globalId) throws JSONException, UnsupportedEncodingException {

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");

        //POST params.
        JSONObject body = new JSONObject();
        body.put("consent", consent);

        if(consent){
            body.put("consentDate", getFormatedDate());
        }
        else{
            body.put("optOutDate", getFormatedDate());
        }

        StringEntity entity = new StringEntity(body.toString());

        client.post(this, urlGDPR + globalId, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                        Log.d(TAG, "[GDPR][GamehouseGDPR] Response sucessfull from the server: " + statusCode);
                    }

                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d(TAG, "[GDPR][GamehouseGDPR] Response failed with code : " + statusCode + " Err desc: "+ error.getLocalizedMessage());
                    }
                });
         }
    /*
        Gets the date with the specific format from the backend
     */
    private String getFormatedDate() {
        //This is the format configured in the backend
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        return df.format(Calendar.getInstance().getTime());
    }
}
