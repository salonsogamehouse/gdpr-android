package com.gamehouse.gdpr;

import android.content.Intent;
/*
    The idea of this class was avoid all the problems with enums I saw on internet with the Android performance (https://android.jlelse.eu/android-performance-avoid-using-enum-on-android-326be0794dc3)
    So I took this idea from here: https://stackoverflow.com/a/9753178
 */

public enum GDPRActivityEnum {

    //Here defines all the enums
    GDPR_CONSENT,
    GDPR_OPT_OUT,
    GDPR_ASK_AGAIN;

    private static final String name = GDPRActivityEnum.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static GDPRActivityEnum detachFrom(Intent intent) {
        if(!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}