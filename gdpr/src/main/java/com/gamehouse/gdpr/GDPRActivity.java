package com.gamehouse.gdpr;

import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.TextView;

public class GDPRActivity extends AppCompatActivity {

    private static final String TAG = "GDPRActivity";
    public static final int GDPR_ACTIVITY_CODE = 1;

    public static int GDPR_CONSENT_OK = 0;
    public static int GDPR_CONSENT_NOK = 1;
    public static int GDPR_OPT_OUT_OK = 2;
    public static int GDPR_OPT_OUT_NOK= 3;
    public static int GDPR_ASK_AGAIN_OK= 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gdpr);

        //Get the type of activity from the caller.
        GDPRActivityEnum result = GDPRActivityEnum.detachFrom(getIntent());

        switch (result)
        {
            case GDPR_CONSENT: {
                createGDPRConsent();
                break;
            }
            case GDPR_OPT_OUT: {
                createGDPROptOut();
                break;
            }
            case GDPR_ASK_AGAIN: {
                createGDPRAskAgain();
                break;
            }
        }
    }

    private void createGDPRConsent(){
        // Linkify the message
        final SpannableString s = new SpannableString(this.getText(R.string.gdpr_consent_message));
        Linkify.addLinks(s, Linkify.ALL);

        ContextThemeWrapper themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light);

        AlertDialog dialogGDPRConsent = new AlertDialog.Builder(themedContext)
                .setTitle(R.string.gdpr_consent_title)
                .setMessage(s)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.agree, new DialogInterface.OnClickListener() {
                        @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GDPRStorage.setGdprConsent(getApplicationContext(), true);
                        Log.d(TAG, "[GDPR][GDPRActivity] Player pressed Agree");


                        Intent intent = new Intent();
                        setResult(GDPR_CONSENT_OK, intent);
                        finish();
                    }
                })
//                .setNegativeButton(R.string.disagree, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        GDPRStorage.setGdprConsent(getApplicationContext(), false);
//                        Log.d(TAG, "[GDPR][GDPRActivity] Player pressed Disagree");
//
//                        Intent intent = new Intent();
//                        setResult(GDPR_CONSENT_NOK, intent);
//                        finish();
//                    }
//                })
                .setCancelable(false)
                .show();

       ((TextView)dialogGDPRConsent.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void createGDPROptOut(){
        // Linkify the message
        final SpannableString s = new SpannableString(this.getText(R.string.gdpr_optout_message));
        Linkify.addLinks(s, Linkify.ALL);

        ContextThemeWrapper themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light);

        AlertDialog dialogGDPROptOut = new AlertDialog.Builder(themedContext)
                .setTitle(R.string.gdpr_optout_title)
                .setMessage(s)
                .setIcon(android.R.drawable.ic_delete)
                .setPositiveButton(R.string.agree_opt_out, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GDPRStorage.setGdprConsent(getApplicationContext(), false);
                        Log.d(TAG, "[GDPR][GDPRActivity] Player wants to Opt-Out!");

                        Intent intent = new Intent();
                        intent.putExtra(Intent.EXTRA_TEXT,"OPT-OUT");
                        setResult(GDPR_OPT_OUT_OK, intent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.disagree_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "[GDPR][GDPRActivity] Player pressed Disagree when opt-out");

                        Intent intent = new Intent();
                        intent.putExtra(Intent.EXTRA_TEXT,"CANCEL-OPT-OUT");
                        setResult(GDPR_OPT_OUT_NOK, intent);
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
        dialogGDPROptOut.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
        ((TextView)dialogGDPROptOut.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void createGDPRAskAgain(){

        ContextThemeWrapper themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light);
        new AlertDialog.Builder(themedContext)
                .setTitle(R.string.gdpr_consent_title)
                .setMessage(R.string.disagree_optIn)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.agree, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        setResult(GDPR_ASK_AGAIN_OK, intent);
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
   }

}
