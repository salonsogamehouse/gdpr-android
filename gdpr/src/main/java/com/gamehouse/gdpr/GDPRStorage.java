package com.gamehouse.gdpr;

import android.content.Context;
import android.content.SharedPreferences;

class GDPRStorage {

    private static final String GAMEHOUSE_GDPR_CONSENT = "gh_gdpr_consent";

    public static boolean getGdprConsent(Context context) {
        return getSharedPreferences(context).getBoolean(GAMEHOUSE_GDPR_CONSENT, false);
    }

    public static void setGdprConsent(Context context, boolean consent) {
        getSharedPreferences(context).edit().putBoolean(GAMEHOUSE_GDPR_CONSENT, consent).apply();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.
                getSharedPreferences(context.getString(R.string.gdpr_prefs), Context.MODE_PRIVATE);
    }
}
