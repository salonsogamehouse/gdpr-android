# GDPR-Android version  

This code in general we cover all the aspects for satisfying the law according to GDPR for all the games. 

We recommend to use this class in the base class that triggers everything. In many games this class is usually GF2BaseActivity but it may be different. Check your project's requirements for that. 

**How to test this library :** just press the play button.

**How to compile/integrate this library:** you can directly link this *.aar file to your project with Gradle. 

Also if you are a console enthusiasm: to build only the GDPR library, you can run ./gradlew :gdpr:assembleRelease on the root path with the command-line. The resulting .aar will be placed in "gdpr/build/outputs/aar/gdpr-release.aar".

 - android.gradle(project): 

    flatDir {  
        dirs '../../../../' + 'thirdparty/gdpr/android/1.0'  
    }

- engine.gradle:

compile(name: 'gdpr-release', ext: 'aar')

- GF2Activity:

    import  com.gamehouse.gdpr.GamehouseGDPR;

We **strongly** recommend to use a centralized method to make all the sdks init in one specific method. We cannot avoid other SDKs to stop initializing but setting them after this  call. 

Because we need to get a native response from the user we need to override the onActivityResult on the caller class. We can do it in this way:

There is an example app that you can download from this project to check the methods needed. 

-*To create a consent flow:*
createGDPRDialog(GDPRActivityEnum.GDPR_CONSENT);  

*-To create an opt-out flow:*  
createGDPRDialog(GDPRActivityEnum.GDPR_OPT_OUT);  

*-To create an ask-again flow:*  
createGDPRDialog(GDPRActivityEnum.GDPR_ASK_AGAIN);

Also check the onActivityResult methods to implement the result coming from the native dialog.

*-If you need to update the backend:*

GamehouseGDPR.getInstance().gdprUpdateBackend(false, "18b380e5-4369-4bc4-a05b-03c1f2b58a8f"); //fake global id
  